import time


# Based off of https://github.com/Rapptz/RoboDanny/blob/rewrite/cogs/utils/cache.py#L22
# MPL-v2 License
class ExpiringDict(dict):

    def __init__(self, seconds):
        self.seconds = seconds
        super().__init__()

    def _check_integrity(self):
        to_remove = []
        current = time.monotonic()
        for k, (v, t) in self.items():
            # If current time is greater than when it was added + the expiring time.
            if current > (t + self.seconds):
                # We just need to add the keys to this.
                to_remove.append(k)
        for k in to_remove:
            del self[k]

    def __setitem__(self, key, value):
        super().__setitem__(key, (value, time.monotonic()))

    def __getitem__(self, key):
        self._check_integrity()
        return super().__getitem__(key)[0]

    def __contains__(self, item):
        self._check_integrity()
        return super().__contains__(item)
