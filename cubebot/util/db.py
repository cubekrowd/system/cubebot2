import pymongo

import cubebot


class Database:

    def __init__(self):
        self.client = None
        self.db = None

    def start(self):
        uri = cubebot.config["mongodb"]["uri"]
        self.client = pymongo.MongoClient(uri, connect=True, maxPoolSize=2, connectTimeoutMS=1000, appname="CubeBot")
        db_name = cubebot.config["mongodb"]["database"]
        self.db = self.client[db_name]

    def __getitem__(self, key):
        prefix = cubebot.config["mongodb"]["collection_prefix"]
        return self.db[prefix + key]
