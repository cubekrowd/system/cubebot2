import asyncio
import yaml

import cubebot
from .util import Database
from .bot import Bot
from .api import APIServer
from .link_manager import LinkManager


print("Running CubeBot")

print("Loading config")
with open("config.yaml") as f:
    cubebot.config = yaml.safe_load(f)

if cubebot.config['debug'] is True:
    print("Debug mode is on!")

cubebot.link_manager = LinkManager()

loop = asyncio.get_event_loop()

print("Setting up database")
cubebot.db = Database()
cubebot.db.start()

print("Loading linked data")
try:
    cubebot.link_manager.load_data()
except Exception as e:
    print(f"Failed to load linked data {e}")
    exit()

print("Logging in to Discord")
cubebot.bot = Bot()
loop.run_until_complete(cubebot.bot.login())

print("Starting API server")
cubebot.api_server = APIServer()
loop.run_until_complete(cubebot.api_server.start())

loop.create_task(cubebot.bot.connect())

try:
    loop.run_forever()
except KeyboardInterrupt:
    loop.run_until_complete(asyncio.gather(
        cubebot.bot.logout(),
        cubebot.bot.clean_webhooks(),
        cubebot.api_server.shutdown()
    ))
finally:
    loop.close()
