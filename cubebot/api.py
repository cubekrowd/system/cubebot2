import asyncio
import traceback
from uuid import UUID
from base64 import urlsafe_b64encode, urlsafe_b64decode

import cubebot
from .util import MemberNotFound, RankNotFound, UUIDNotFound, AlreadyLinked


class APIServer:

    def __init__(self):
        self.server = None
        self.handlers = {
            'LINK': self._handle_link,
            'UNLINK': self._handle_unlink,
            'DATA': self._handle_data,
            'FORCELINK': self._handle_forcelink
        }

    async def start(self):
        """
        Start the server to handle linkup information.
        """
        self.server = await asyncio.start_server(self.handle, "127.0.0.1", 26701, limit=1024)
        address = self.server.sockets[0].getsockname()
        print(f"Serving on {address[0]}:{address[1]}")

    async def handle(self, reader, writer):
        # continuously read from socket, until client closes the connection
        while True:
            data = await reader.readline()
            if not data.endswith(b'\n'):
                # EOF reached, message not complete
                writer.close()
                await writer.wait_closed()
                return
            cmd = data.decode(errors='replace').strip()
            res = await self.handle_msg(cmd) + '\n'
            print('sending', res)
            if res is not None:
                writer.writelines([res.encode()])
                await writer.drain()

    async def handle_msg(self, data):
        """
        Handle data received from the server.
        """
        print(f"Received {data}")

        # The "request identifier" is the part of the request up to the first
        # colon. It tells us which handler to use and is case-sensitive.
        # N.B. if the colon is *not* found, then the entire string is used
        # as the request identifier.
        sep_index = data.find(':')
        request_identifier = data[:sep_index]
        if request_identifier not in self.handlers:
            return "ERROR:ARGS"

        # Split up arguments.
        if sep_index != -1:
            unsplit_args = data[sep_index + 1:]
            request_args = tuple(unsplit_args.split('|'))
        else:
            request_args = ()

        # Retrieve the correct handler for this request.
        handler = self.handlers[request_identifier]

        # TODO: it would be nice for handlers to be able to specify type
        # hints, and then we could use the "inspect" module here to verify
        # that the correct number of args were passed and that they were all
        # of the correct type (e.g. UUIDs are restricted to certain formats).
        # But since this is an internal-only API, it's not a big deal.

        # Execute the handler with the request arguments (these are passed in
        # the same order that they were received in request_args).
        # If the handler fails, print the error in the console and send an
        # error message to the client.
        try:
            return await handler(*request_args)
        except MemberNotFound:
            return "ERROR:INCORRECT_USER"
        except RankNotFound:
            return "ERROR:RANK"
        except UUIDNotFound:
            return "ERROR:UNKNOWN_UUID"
        except AlreadyLinked:
            return "ERROR:ALREADY_LINKED"
        except Exception as e:
            print(f'Failed to handle {data}:\n' + traceback.format_exc())
            return "ERROR:UNKNOWN"

    async def _handle_forcelink(self, uuid, ranks, ign, discord_name):
        '''Handler for the FORCELINK request.

        Returns the response to send back:
            LINKED:<discord_name> = this IGN/uuid is now linked to the given
                discord name.
            CODE:<code> = this IGN/uuid can be linked using the given code.
            ERROR:INCORRECT_USER = user is not in the guild
            ERROR:UNKNOWN = some other error
        '''
        # FORCELINK:<UUID>|<Ranks>|<IGN>|<Username#0000>

        # ranks need to be split up by & before processing...
        ranks = ranks.split('&')
        if ranks == ['']:
            ranks = []
        try:
            uuid = UUID(uuid)
        except ValueError as e:
            traceback.print_exc()
            return "ERROR:UUID"
        try:
            discord_name = urlsafe_b64decode(discord_name).decode('utf-8')
        except Exception as e:
            return "ERROR:INCORRECT_USER"

        user_link = cubebot.link_manager.get_member(discord_name)

        await cubebot.link_manager.linkup_or_update(uuid, ign, ranks, user_link)

        return f'LINKED:{discord_name}'

    async def _handle_link(self, uuid, ranks, ign, discord_name):
        '''Handler for the LINK request.

        Returns the response to send back:
            LINKED:<discord_name> = this IGN/uuid is now linked to the given
                discord name.
            CODE:<code> = this IGN/uuid can be linked using the given code.
            ERROR:INCORRECT_USER = user is not in the guild
            ERROR:UNKNOWN = some other error
        '''
        # LINK:<UUID>|<Ranks>|<IGN>|<Username#0000>

        # ranks need to be split up by & before processing...
        ranks = ranks.split('&')
        if ranks == ['']:
            ranks = []
        try:
            uuid = UUID(uuid)
        except ValueError as e:
            traceback.print_exc()
            return "ERROR:UUID"
        try:
            discord_name = urlsafe_b64decode(discord_name).decode('utf-8')
        except Exception as e:
            return "ERROR:INCORRECT_USER"

        code, name = await cubebot.link_manager.request_code(uuid, ranks, ign, discord_name)

        if code is None:
            # @NOTE(traks) discord usernames have length 2-32 plus 5 for the
            # discriminating number. Could also be the ID
            return f"LINKED:{name}"
        else:
            return f"CODE:{code}"

    async def _handle_data(self, uuid, ranks, ign):
        # DATA:<UUID>|<Ranks>|<IGN>
        required_ranks = ranks.split('&')
        if required_ranks == ['']:
            required_ranks = []
        try:
            uuid = UUID(uuid)
        except ValueError as e:
            traceback.print_exc()
            return "ERROR:UUID"

        await cubebot.link_manager.update_data(uuid, required_ranks, ign)

        return "OK"

    async def _handle_unlink(self, uuid):
        # UNLINK:<UUID>
        try:
            uuid = UUID(uuid)
        except ValueError as e:
            traceback.print_exc()
            return "ERROR:UUID"

        discord_name = await cubebot.link_manager.unlink(uuid)

        if discord_name is None:
            return "ERROR:NOTLINKED"
        else:
            return f"UNLINKED:{discord_name}"

    async def shutdown(self):
        self.server.close()
        await self.server.wait_closed()
