from asyncio import Lock
from uuid import UUID

import discord
from pymongo.write_concern import WriteConcern

import cubebot
import string
import secrets
from .util import cache, MemberNotFound, RankNotFound, AlreadyLinked, UUIDNotFound
from functools import wraps


def get_random_string(length):
    """
    Gets a string of random lowercase letters.
    :param length: Length of string.
    :return: Random string that was generated.
    """
    letters = string.ascii_lowercase
    result_str = ''.join(secrets.choice(letters) for _ in range(length))
    return result_str


def lock_uuid(func):
    """
    Wrapper to only use UUID once at a time. Function has to have the first arg as a class
    and the second argument as a UUID.
    """
    @wraps(func)
    async def wrapper(*args):
        self = args[0]
        uuid = args[1]
        lock = self.uuid_locks.get(uuid)
        if lock is None:
            lock = Lock()
            lock.waiters = 0
            self.uuid_locks[uuid] = lock

        lock.waiters = lock.waiters + 1
        await lock.acquire()
        try:
            return await func(*args)
        finally:
            lock.release()
            lock.waiters = lock.waiters - 1
            if lock.waiters == 0:
                del self.uuid_locks[uuid]

    return wrapper


# @NOTE(traks) There are a few things that should be taken into account. First
# of all, apparently Discord is so distributed/concurrent that it doesn't
# provide a whole lot of consistency guarantees. E.g. the same event may be
# received multiple times or not at all. The bot may also get disconnected
# randomly and therefore may miss events. If CubeBot crashes/reboots, we of
# course also miss events.
#
# Since I haven't seen issues such as undeleted messages with the old CubeBot, I
# consider all of this not very likely. But it's worthwhile to note that
# theoretically, someone could hijack someone else's linkup code if CubeBot
# doesn't detect the original linkup message.
#
# @TODO(traks) perhaps there are ways to avoid hijacking codes. Although it
# seems these kinds of exploits are unavoidable with our current system...
#
# Secondly, Discord users may leave our server at any point in time, and we may
# miss such events. This means that our linked collection could contain entries
# for Discord accounts that aren't on the server anymore. In other words, being
# in the database =/=> user has "Linked" role. However, we can and do ensure
# that having the "Linked" role ==> user is in database: it would be pointless
# if we didn't know the UUID of a Discord user with the "Linked" role.
#
# Thirdly, the bot framework we're using caches a lot of things, such as the
# guild member list, member roles and nick names. This is good, because we want
# to avoid hitting the rate limit, which is pretty low for Discord I believe.
# However, the way caching works isn't very ideal. For example, editing a member
# will change the cached roles locally and the framework will send and update to
# Discord, but if the Discord update fails the locally cached ranks won't be
# reset. Hence the cached data can be out-dated and can even be in a state that
# Discord never knew about.
class LinkManager:

    def __init__(self):
        # Have values expire after 15 minutes
        self.to_link = cache.ExpiringDict(60 * 15)
        self.rank_map = {}
        config_ranks = cubebot.config["ranks"]
        for r in config_ranks:
            self.rank_map[r] = config_ranks[r]["role-ids"]
        # local database copy with ign & ranks
        self.linked_data = {}
        self.uuid_by_discord = {}
        self.uuid_locks = {}

    def load_data(self):
        """
        Loads linkup data from the database and stores discord id -> uuid in self.uuid_by_discord, and
        uuid -> discord id/ign.
        """
        col = cubebot.db["linked"]
        for doc in col.find({}):
            uuid = UUID(doc["_id"])
            discord_id = doc["discord_id"]
            self.linked_data[uuid] = {
                'discord_id': discord_id,
                'ign': doc['last_ign']
            }
            self.uuid_by_discord[discord_id] = uuid

    def get_member(self, name):
        '''Get a d.py Member from a 'name#0000' string or a 'member id' string.

        Raises MemberNotFound if no such Member exists.'''

        guild: discord.Guild = cubebot.bot.get_guild(cubebot.config["guild-id"])

        if '#' not in name:
            # try to interpret it as a member id
            try:
                id = int(name)
            except ValueError:
                raise MemberNotFound()

            member = guild.get_member(id)
        else:
            # try to interpret it as a user#0000
            member = None
            for u in guild.members:
                if str(u) == name:
                    member = u
                    break
        # If user is not in Guild, send an error.
        if member is None:
            raise MemberNotFound()

        return member

    @lock_uuid
    async def request_code(self, uuid, ranks, ign, user):
        """
        Creates a linkup code with data stored in self.to_link
        :param uuid: Minecraft UUID
        :param ranks: Minecraft Ranks
        :param ign: Minecraft IGN
        :param user: Discord user name + discriminator (Username#0000)
        :return: A tuple with code and name.
            - If no code exists or they are currently linking up, it returns the linkup code/None.
            - If the person is already linked up it returns None/Found member username and
              discriminator, or just user id.
        """
        guild: discord.Guild = cubebot.bot.get_guild(cubebot.config["guild-id"])

        user_link = self.get_member(user)
        # Don't add two links for one discord user
        if user_link.id in self.uuid_by_discord:
            raise AlreadyLinked()

        # We only want the UUID to be in here once.
        code = None
        for link in self.to_link:
            data = self.to_link[link]
            if uuid == data['uuid']:
                code = link
                break

        if code is None:
            data = self.linked_data.get(uuid)
            if data:
                discord_id = data['discord_id']
                member = guild.get_member(discord_id)
                if member is None:
                    return None, str(discord_id)
                else:
                    return None, str(member)

            while True:
                # Chances of a repeat is super low, but still, can never be too
                # safe :) (Don't want to give someone admin perms :P)
                code = get_random_string(6)
                if code not in self.to_link:
                    break

        self.to_link[code] = {
            'uuid': uuid,
            'ign': ign,
            'ranks': ranks,
            'user': user_link
        }
        return code, None

    def delete_link_unchecked(self, uuid):
        """
        Deletes linkup data for given UUID. Database and dicts stored.
        :param uuid: Minecraft UUID
        """
        # Just fire and forget. Not a big deal if they remain in the
        # database. When we do the actual linking, it'll be overwritten
        # either way.
        col = cubebot.db["linked"].with_options(write_concern=WriteConcern(w=0))
        col.delete_one({'_id': str(uuid)})
        data = self.linked_data[uuid]
        del self.uuid_by_discord[data['discord_id']]
        del self.linked_data[uuid]

    def get_unmanaged_roles(self, member: discord.Member):
        """
        Gets roles from member that are not handled by linkup.
        :param member: Member to get roles from.
        :return: List of discord.Role.
        """
        managed_ids = []
        for r in self.rank_map:
            managed_ids.extend(self.rank_map[r])
        managed_ids.append(cubebot.config["linked-role-id"])

        member_roles = member.roles
        # Remove roles the player has that we manage
        return [r for r in member_roles if r.id not in managed_ids]

    def recalculate_roles(self, member: discord.Member, ranks):
        """
        Gets the roles that the member should have based off of their current roles and their rank.
        :param member: Member to get new roles.
        :param ranks: List of ranks.
        :return: List of discord.Roles that the member should have.
        """
        new_roles = set(self.get_unmanaged_roles(member))
        # add back the roles we manage
        for r in ranks:
            # ignore unknown ranks, these may not be useful for us
            if r not in self.rank_map:
                continue
            for rid in self.rank_map[r]:
                role = member.guild.get_role(rid)
                if role:
                    new_roles.add(role)

        linked_role = member.guild.get_role(cubebot.config["linked-role-id"])
        if linked_role:
            new_roles.add(linked_role)
        return list(new_roles)

    @lock_uuid
    async def update_data(self, uuid, ranks, ign):
        """
        Updates user data in database and discord. If user has yet to finish linking up it will update their data
        as well.
        :param uuid: Minecraft UUID
        :param ranks: Minecraft Ranks
        :param ign: Minecraft IGN
        """
        # try to update data in to_link first
        for code in self.to_link:
            data = self.to_link[code]
            if uuid == data['uuid']:
                # FIXME: LuckPerms gives us lowercase usernames, which won't
                # work for us and that's where this would happen.
                if ign == '':
                    ign = data['ign']
                self.to_link[code] = {
                    'uuid': uuid,
                    'ign': ign,
                    'ranks': ranks,
                    'user': data['user']
                }
                return

        if uuid not in self.linked_data:
            raise UUIDNotFound()
        data = self.linked_data[uuid]

        # FIXME: LuckPerms gives us lowercase usernames, which won't
        # work for us and that's where this would happen.
        if ign == '':
            ign = data['ign']

        guild = cubebot.bot.get_guild(cubebot.config["guild-id"])
        member = guild.get_member(data['discord_id'])
        if member is None:
            raise MemberNotFound()

        old_roles = member.roles
        new_roles = self.recalculate_roles(member, ranks)

        if set(old_roles) == set(new_roles) and ign == member.display_name:
            # nothing changed
            return

        try:
            await member.edit(nick=ign, roles=new_roles, reason="CubeBot update")
        except Exception as e:
            print(f"Failed to update member data for {ign}: {e}")
            raise Exception()

        data['ign'] = ign
        try:
            # if this fails, tough luck...
            col = cubebot.db["linked"].with_options(write_concern=WriteConcern(w=0))
            col.update_one({'_id': str(uuid)}, {'$set': {'last_ign': ign}})
        except Exception as e:
            print(f"Failed to save member data for {ign}: {e}")
            pass

    def remove_data(self, member: discord.Member):
        """
        Removes data from member. Same as @method.delete_link_unchecked, but checks to make sure
        they exist in the system first.
        :param member: Member to remove
        """
        uuid = self.uuid_by_discord.get(member.id)
        if uuid:
            self.delete_link_unchecked(uuid)

    def get_ign(self, member: discord.Member):
        """
        Gets IGN from member.
        :param member: Member
        :return: Minecraft IGN
        """
        uuid = self.uuid_by_discord.get(member.id)
        if uuid:
            return self.linked_data[uuid]['ign']
        else:
            return None

    @lock_uuid
    async def unlink(self, uuid):
        """
        Unlinks a player completely from discord, as well as the database.
        :param uuid: Minecraft UUID
        :return: Member name + discriminator.
        """

        if uuid not in self.linked_data:
            # No data ==> no "Linked" role
            return None
        data = self.linked_data[uuid]

        guild = cubebot.bot.get_guild(cubebot.config["guild-id"])
        member = guild.get_member(data['discord_id'])
        if member is None:
            raise MemberNotFound()

        new_roles = self.get_unmanaged_roles(member)

        # If this throws, we can't be sure if the roles actually changed or
        # not, so keep the link in the database
        # We don't change the nick here because it will revert right back because the database still has it.
        await member.edit(roles=new_roles, reason="CubeBot unlink")

        self.delete_link_unchecked(uuid)
        return str(member)

    async def unlink_from_discord(self, member: discord.Member):
        """Same as unlink, but uses a discord Member instead."""
        uuid = self.uuid_by_discord.get(member.id)

        if uuid is None:
            return None
        return await self.unlink(uuid)

    @lock_uuid
    async def linkup_explicit(self, uuid, ign, ranks, member: discord.Member):
        """
        Linkup member/player with all specified data. No codes are used in this method.
        :param uuid: Minecraft UUID
        :param ign: Minecraft IGN
        :param ranks: Minecraft Ranks
        :param member: Member to link to
        :return: IGN
        """
        # Don't add two links for one discord user
        if member.id in self.uuid_by_discord:
            raise AlreadyLinked()
        # Add link to database
        col = cubebot.db["linked"].with_options(write_concern=WriteConcern(j=True))
        col.update_one({"_id": str(uuid)}, {
            "$set": {"discord_id": member.id, 'last_ign': ign}
        }, upsert=True)
        self.linked_data[uuid] = {
            'discord_id': member.id,
            'ign': ign
        }
        self.uuid_by_discord[member.id] = uuid

        # Set user information in the guild
        new_roles = self.recalculate_roles(member, ranks)

        await member.edit(nick=ign, roles=new_roles)
        return ign

    async def linkup(self, code, member: discord.Member):
        """
        Linkups member based off of a code.
        :param code: Linkup code
        :param member: Member to link to
        :return: None if they don't have a code, IGN if they linked up successfully.
        """
        if code not in self.to_link:
            return None
        data = self.to_link[code]
        # The users have to be the same.
        if data['user'] is not member:
            raise MemberNotFound()
        del self.to_link[code]
        uuid = data['uuid']
        ign = data['ign']
        ranks = data['ranks']
        return await self.linkup_explicit(uuid, ign, ranks, member)

    async def linkup_or_update(self, uuid, ign, ranks, member: discord.Member):
        '''
        Linkup member/player with all specified data. No codes are used in this method.
        :param uuid: Minecraft UUID
        :param ign: Minecraft IGN
        :param ranks: Minecraft Ranks
        :param member: Member to link to
        '''
        # If we are switching Minecraft UUIDs, we should unlink first.
        if member.id in self.uuid_by_discord and self.uuid_by_discord[member.id] != uuid:
            await self.unlink_from_discord(member)

        # If, now, we are already linked up, update the data. Otherwise,
        # create a new linkup.
        if uuid in self.linked_data:
            await self.update_data(uuid, ranks, ign)
        else:
            await self.linkup_explicit(uuid, ign, ranks, member)

    async def get_data(self, member: discord.Member):
        """
        Returns a dictionary with uuid and ign based off of a member.
        :param member: Member to get data on.
        :return: Dictionary with data. Returns None if nothing was found.
        """
        for k, v in self.linked_data.items():
            if v['discord_id'] == member.id:
                return {
                    'uuid': k,
                    'ign': v['ign']
                }
        return None
