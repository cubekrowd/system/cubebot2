import discord
from discord.ext import commands

import cubebot
from cubebot.util import MemberNotFound
from cubebot.util.context import Context


class Linkup(commands.Cog):

    @commands.Cog.listener()
    async def on_message(self, message: discord.Message):
        if message.author.id == cubebot.bot.user.id:
            return

        if message.channel.id != cubebot.config["linkup-channel-id"]:
            return

        code = message.content
        author = message.author
        channel = message.channel
        await message.delete()
        if code.startswith('!linkid'):
            return

        try:
            ign = await cubebot.link_manager.linkup(code, author)
        except Exception as e:
            if isinstance(e, MemberNotFound):
                return await channel.send(f"{author.mention} That is someone else's code! Please check that you "
                                          f"put in your Discord name correctly in game or contact a staff member.")
            print(f"Failed to link up: {e}")
            return await channel.send(f"{author.mention} Something went wrong! Please try linking up again or "
                                      f"contact a staff member.", delete_after=30)

        if ign is None:
            return await channel.send(f"{author.mention} That's an invalid code. Use `/discord linkup {author.id}` in "
                                      f"game to get a new linkup code.", delete_after=30)
        else:
            return await channel.send(f"{author.mention} Linked Minecraft account `{ign}`.", delete_after=30)

    @commands.command(name="unlink")
    async def unlink(self, ctx: Context):
        ign = await cubebot.link_manager.unlink_from_discord(ctx.author)
        if ign is None:
            await ctx.send(f"{ctx.author.mention} You're not linked!", delete_after=10)
        else:
            await ctx.send(f"{ctx.author.mention} Successfully unlinked!", delete_after=10)

    @commands.command(name="linkid")
    async def linkid(self, ctx: Context):
        if ctx.channel.id != cubebot.config["linkup-channel-id"]:
            return
        await ctx.send(f"{ctx.author.mention} Your ID is `{ctx.author.id}`.\n"
                       f"You can link up in game using `/discord linkup {ctx.author.id}`!", delete_after=30)

    @commands.command(name="send")
    @cubebot.debug_only()
    async def send(self, ctx: Context, *, data):
        if data is None:
            return await ctx.send("Specify data please.")

        response = await cubebot.api_server.handle_msg(data)
        if response is not None:
            await ctx.send(f"```{response}```")

    @commands.command(name="getdata")
    @cubebot.debug_only()
    async def get_data(self, ctx: Context, user: discord.Member):
        """
        Gets data about specified member.
        """
        if user is None:
            return await ctx.send("Specify user please.")
        data = await cubebot.link_manager.get_data(user)
        if data is None:
            return await ctx.send("No data for that user.")
        embed = discord.Embed(
            title=str(user),
            description=f"User ID: `{user.id}`\nUser UUID: `{data['uuid']}`\nIGN: `{data['ign']}`",
            colour=discord.Colour.orange()
        )
        await ctx.send(embed=embed)

    @commands.command(name="ranks")
    @cubebot.debug_only()
    async def get_ranks(self, ctx: Context):
        """
        Displays the current ranks
        """
        message = ""
        rank_map = cubebot.link_manager.rank_map
        for r in rank_map:
            message = message + f"{r} - `{rank_map[r]}`\n"

        await ctx.send(message)
