from discord.ext import commands


# globals
config = None
db = None
link_manager = None
api_server = None
bot = None


# for command decorators
def is_debug(*args):
    return config["debug"]


def debug_only():
    def predicate(ctx):
        return is_debug()

    return commands.check(predicate)
